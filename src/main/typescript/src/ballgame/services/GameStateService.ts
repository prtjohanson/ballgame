import { Subject, throwError, timer } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { concatMap, catchError, map } from 'rxjs/operators';
import { Game, isGame } from '../interfaces/Game';
import { Container } from "../../ioc/Container";  // adjust the import path as necessary

export class GameStateService {
    private subject = new Subject<Game>();

    private container: Container

    private url: string;
    private delayMs: number;

    constructor(container: Container) {
        this.container = container;

        this.url = container.getConfig('service.gameState.url', 'string') as string;
        this.delayMs = container.getConfig('service.gameState.delayMs', 'number') as number;
    }

    startFetchingData() {
        timer(0, this.delayMs).pipe(
            concatMap(() => ajax.getJSON(this.url)),
            map(data => {
                if (isGame(data)) {
                    this.subject.next(data as Game);
                } else {
                    throw new Error('Data does not match Game structure');
                }
            }),
            catchError(error => {
                console.error('Error fetching game data:', error);
                return throwError(error);
            })
        ).subscribe();
    }

    getGame() {
        return this.subject.asObservable();
    }
}