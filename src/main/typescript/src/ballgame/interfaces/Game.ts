import { Field, isField } from "./Field";
export interface Game {
    blueTeamName: string;
    blackTeamName: string;
    field: Field;
    blueTeamScore: number;
    blackTeamScore: number;
}

export function isGame(arg: unknown): arg is Game {
    if (typeof arg !== 'object' || arg === null) return false;
    const game = arg as Game;
    return typeof game.blueTeamName === 'string' &&
        typeof game.blackTeamName === 'string' &&
        isField(game.field) &&
        typeof game.blueTeamScore === 'number' &&
        typeof game.blackTeamScore === 'number';
}
