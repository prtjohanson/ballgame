import { Ball, isBall } from "./Ball";
import { Goal, isGoal } from "./Goal";
import { Player, isPlayer } from "./Player";

export interface Field {
    ball: Ball;
    blackTeamGoal: Goal;
    blueTeamGoal: Goal;
    blackTeamPlayers: Player[];
    blueTeamPlayers: Player[];
    width: number;
    height: number;
    xPosition: number;
    yPosition: number;
    friction: number;
}

export function isField(arg: unknown): arg is Field {
    if (typeof arg !== 'object' || arg === null) return false;
    const field = arg as Field;
    return isBall(field.ball) &&
        isGoal(field.blackTeamGoal) &&
        isGoal(field.blueTeamGoal) &&
        Array.isArray(field.blackTeamPlayers) && field.blackTeamPlayers.every(isPlayer) &&
        Array.isArray(field.blueTeamPlayers) && field.blueTeamPlayers.every(isPlayer) &&
        typeof field.width === 'number' &&
        typeof field.height === 'number' &&
        typeof field.xPosition === 'number' &&
        typeof field.yPosition === 'number' &&
        typeof field.friction === 'number';
}
