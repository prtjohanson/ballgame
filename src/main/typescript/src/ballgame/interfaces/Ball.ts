export interface Ball {
    radius: number;
    xPosition: number;
    yPosition: number;
    xVelocity: number;
    yVelocity: number;
    friction: number;
}

export function isBall(arg: unknown): arg is Ball {
    if (typeof arg !== 'object' || arg === null) return false;
    const ball = arg as Ball;
    return typeof ball.radius === 'number' &&
        typeof ball.xPosition === 'number' &&
        typeof ball.yPosition === 'number' &&
        typeof ball.xVelocity === 'number' &&
        typeof ball.yVelocity === 'number' &&
        typeof ball.friction === 'number';
}