export interface Goal {
    width: number;
    height: number;
    xPosition: number;
    yPosition: number;
}

export function isGoal(arg: unknown): arg is Goal {
    if (typeof arg !== 'object' || arg === null) return false;
    const goal = arg as Goal;
    return typeof goal.width === 'number' &&
        typeof goal.height === 'number' &&
        typeof goal.xPosition === 'number' &&
        typeof goal.yPosition === 'number';
}