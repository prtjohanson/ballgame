export interface Player {
    radius: number;
    xPosition: number;
    yPosition: number;
    xVelocity: number;
    yVelocity: number;
    friction: number;
    name: string;
}

export function isPlayer(arg: unknown): arg is Player {
    if (typeof arg !== 'object' || arg === null) return false;
    const player = arg as Player;
    return typeof player.radius === 'number' &&
        typeof player.xPosition === 'number' &&
        typeof player.yPosition === 'number' &&
        typeof player.xVelocity === 'number' &&
        typeof player.yVelocity === 'number' &&
        typeof player.friction === 'number' &&
        typeof player.name === 'string';
}
