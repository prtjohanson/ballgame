import Canvas from "../../Canvas";
export interface Renderer<T> {
    setCanvas(canvas: Canvas): void;
    getCanvas(): Canvas;

    setItem(items: T): Renderer<T>;
    getItem(): T;

    setStrokeColor(color: string): Renderer<T>;
    getStrokeColor(): string;

    setFillColor(color: string): Renderer<T>;
    getFillColor(): string;

    render(): void;
}