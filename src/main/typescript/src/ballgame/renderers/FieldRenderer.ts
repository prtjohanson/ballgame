import Canvas from "../../Canvas";
import { Renderer } from "../interfaces/Renderer";
import { Field } from "../interfaces/Field";
import { Container } from "../../ioc/Container";
import { Goal } from "../interfaces/Goal";
import { Ball } from "../interfaces/Ball";
import { Player } from "../interfaces/Player";

export class FieldRenderer implements Renderer<Field> {
    private canvas: Canvas | undefined
    private field: Field | undefined
    private goalRenderer: Renderer<Goal>;
    private ballRenderer: Renderer<Ball>;
    private playerRenderer: Renderer<Player>;
    private strokeStyle: string = "white";
    private fillStyle: string = "green";

    constructor(container: Container) {
        this.canvas = container.get('canvas');
        this.goalRenderer = container.get('renderer.goal');
        this.ballRenderer = container.get('renderer.ball');
        this.playerRenderer = container.get('renderer.player');
    }

    setCanvas(canvas: Canvas): void {
        this.canvas = canvas;
    }

    getCanvas(): Canvas {
        if (!this.canvas) {
            throw new Error('No canvas to get');
        }

        return this.canvas;
    }

    getItem(): Field {
        if (!this.field) {
            throw new Error('No field to get');
        }

        return this.field;
    }

    setItem(field: Field): Renderer<Field> {
        this.field = field;
        return this;
    }

    render() {
        const field = this.getItem();

        const canvasContext = this.getCanvas().getContext();

        canvasContext.strokeStyle = "white";

        canvasContext.strokeRect(field.xPosition, field.yPosition, field.width, field.height);

        this.goalRenderer.setItem(field.blackTeamGoal)
            .setStrokeColor('black')
            .setFillColor('green')
            .render();

        this.goalRenderer.setItem(field.blueTeamGoal)
            .setStrokeColor('blue')
            .setFillColor('green')
            .render();

        this.ballRenderer.setItem(field.ball)
            .setStrokeColor('black')
            .setFillColor('white')
            .render();

        field.blackTeamPlayers.map(
            (blackTeamPlayer) => this.playerRenderer
                .setItem(blackTeamPlayer)
                .setStrokeColor('white')
                .setFillColor('black')
                .render()
        )

        field.blueTeamPlayers.map(
            (blueTeamPlayer) => this.playerRenderer
                .setItem(blueTeamPlayer)
                .setStrokeColor('white')
                .setFillColor('blue')
                .render()
        )
    }

    getFillColor(): string {
        return this.fillStyle;
    }

    getStrokeColor(): string {
        return this.strokeStyle;
    }

    setFillColor(color: string) {
        this.fillStyle = color;

        return this;
    }

    setStrokeColor(color: string) {
        this.strokeStyle = color;

        return this;
    }
}
