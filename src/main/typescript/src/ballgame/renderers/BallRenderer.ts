import { Renderer } from "../interfaces/Renderer";
import { Ball } from "../interfaces/Ball";
import Canvas from "../../Canvas";
import { Container } from "../../ioc/Container";

export class BallRenderer implements Renderer<Ball> {
    private canvas: Canvas | undefined;
    private ball: Ball | undefined;
    private strokeStyle: string = "black";
    private fillStyle: string = "white";

    constructor(container: Container) {
        this.canvas = container.get('canvas');
    }

    setCanvas(canvas: Canvas): void {
        this.canvas = canvas;
    }

    getCanvas(): Canvas {
        if (!this.canvas) {
            throw new Error('No canvas to get');
        }

        return this.canvas;
    }

    getItem(): Ball {
        if (!this.ball) {
            throw new Error('No ball to get');
        }

        return this.ball;
    }

    setItem(ball: Ball): Renderer<Ball> {
        this.ball = ball;
        return this;
    }

    getFillColor(): string {
        return this.fillStyle;
    }

    getStrokeColor(): string {
        return this.strokeStyle;
    }

    setFillColor(color: string) {
        this.fillStyle = color;
        return this;
    }

    setStrokeColor(color: string) {
        this.strokeStyle = color;
        return this;
    }

    render() {
        const ball = this.getItem();

        const canvasContext = this.getCanvas().getContext();

        canvasContext.strokeStyle = this.getStrokeColor();
        canvasContext.fillStyle = this.getFillColor();

        canvasContext.beginPath();
        canvasContext.arc(ball.xPosition, ball.yPosition, ball.radius, 0, Math.PI * 2, false);
        canvasContext.fill();
        canvasContext.stroke();
    }
}
