import Canvas from "../../Canvas";
import { Renderer } from "../interfaces/Renderer";
import { Game } from "../interfaces/Game";
import { Field } from "../interfaces/Field";
import { Container } from "../../ioc/Container";

export class GameRenderer implements Renderer<Game> {
    private canvas: Canvas | undefined
    private game: Game | undefined
    private strokeStyle: string = "white";
    private fillStyle: string = "green";
    private readonly fieldRenderer: Renderer<Field> | undefined;

    constructor(container: Container) {
        this.fieldRenderer = container.get('renderer.field');
        this.canvas = container.get('canvas');
    }

    setCanvas(canvas: Canvas): void {
        this.canvas = canvas;

        this.fieldRenderer ?
            this.fieldRenderer.setCanvas(canvas) :
            this.throwFieldRendererNotProvided();
    }

    getCanvas(): Canvas {
        if (!this.canvas) {
            throw new Error('No canvas to get');
        }

        return this.canvas;
    }

    getItem(): Game {
        if (!this.game) {
            throw new Error('No game to get');
        }

        return this.game;
    }

    setItem(game: Game): Renderer<Game> {
        this.game = game;

        return this;
    }

    render() {
        if (!this.game) {
            throw new Error('No game to render');
        }

        const canvas = this.getCanvas();

        const canvasContext = canvas.getContext();

        canvasContext.fillStyle = "green";

        canvasContext.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        const topMargin = 0.1;
        const bottomMargin = 0.1

        const leftMargin = 0.1;
        const rightMargin = 0.1;

        const fieldWidth = this.game.field.width;
        const fieldHeight = this.game.field.height;

        const scaleWidth = (canvas.getWidth() * (1.0 - leftMargin - rightMargin)) / fieldWidth;
        const scaleHeight = (canvas.getHeight() * (1.0 - topMargin - bottomMargin)) / fieldHeight;

        const scale = scaleWidth < scaleHeight ? scaleWidth
             : scaleHeight;

        const horizontalSpaceLeftOver = canvas.getWidth() -
            (canvas.getWidth() * (leftMargin + rightMargin) + fieldWidth*scale);

        const verticalSpaceLeftOver = canvas.getHeight() -
            (canvas.getHeight() * (topMargin + bottomMargin) + fieldHeight*scale);

        const translateX = canvas.getWidth() * leftMargin +
            (horizontalSpaceLeftOver > 0 ? horizontalSpaceLeftOver * 0.5 : 0);

        const translateY = canvas.getHeight() * topMargin +
            (verticalSpaceLeftOver > 0 ? verticalSpaceLeftOver * 0.5 : 0);

        canvasContext.translate(translateX, translateY);

        canvasContext.scale(scale, scale);

        this.fieldRenderer ?
            this.fieldRenderer.setItem(this.game.field).render() :
            this.throwFieldRendererNotProvided();
    }

    getFillColor(): string {
        return this.fillStyle;
    }

    getStrokeColor(): string {
        return this.strokeStyle;
    }

    setFillColor(color: string) {
        this.fillStyle = color;

        return this;
    }

    setStrokeColor(color: string) {
        this.strokeStyle = color;

        return this;
    }

    protected throwFieldRendererNotProvided(): void {
        throw new Error('Field renderer not provided. DI id: renderer.field')
    }
}
