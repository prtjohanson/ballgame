import Canvas from "../../Canvas";
import { Renderer } from "../interfaces/Renderer";
import { Goal } from "../interfaces/Goal";
import { Container } from "../../ioc/Container";

export class GoalRenderer implements Renderer<Goal> {
    private canvas: Canvas | undefined;
    private goal: Goal | undefined;
    private strokeStyle: string = "white";
    private fillStyle: string = "green";

    constructor(container: Container) {
        this.canvas = container.get('canvas');
    }

    setCanvas(canvas: Canvas): void {
        this.canvas = canvas;
    }

    getCanvas(): Canvas {
        if (!this.canvas) {
            throw new Error('No canvas to get');
        }

        return this.canvas;
    }

    getItem(): Goal {
        if (!this.goal) {
            throw new Error('No goal to get');
        }

        return this.goal;
    }

    setItem(goal: Goal): Renderer<Goal> {
        this.goal = goal;
        return this;
    }

    getFillColor(): string {
        return this.fillStyle;
    }

    getStrokeColor(): string {
        return this.strokeStyle;
    }

    setFillColor(color: string) {
        this.fillStyle = color;

        return this;
    }

    setStrokeColor(color: string) {
        this.strokeStyle = color;

        return this;
    }

    render() {
        const goal = this.getItem();

        const canvasContext = this.getCanvas().getContext();

        canvasContext.strokeStyle = this.getStrokeColor();
        canvasContext.fillStyle = this.getFillColor();

        canvasContext.fillRect(goal.xPosition, goal.yPosition, goal.width, goal.height);
        canvasContext.strokeRect(goal.xPosition, goal.yPosition, goal.width, goal.height);
    }
}
