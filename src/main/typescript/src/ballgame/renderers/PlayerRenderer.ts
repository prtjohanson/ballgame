import { Renderer } from "../interfaces/Renderer";
import { Player } from "../interfaces/Player";
import Canvas from "../../Canvas";
import { Container } from "../../ioc/Container";

export class PlayerRenderer implements Renderer<Player> {
    private canvas: Canvas | undefined;
    private player: Player | undefined;
    private strokeStyle: string = "blue";
    private fillStyle: string = "white";

    constructor(container: Container) {
        this.canvas = container.get('canvas');
    }

    setCanvas(canvas: Canvas): void {
        this.canvas = canvas;
    }

    getCanvas(): Canvas {
        if (!this.canvas) {
            throw new Error('No canvas to get');
        }

        return this.canvas;
    }

    getItem(): Player {
        if (!this.player) {
            throw new Error('No player to get');
        }

        return this.player;
    }

    setItem(player: Player): Renderer<Player> {
        this.player = player;
        return this;
    }

    getFillColor(): string {
        return this.fillStyle;
    }

    getStrokeColor(): string {
        return this.strokeStyle;
    }

    setFillColor(color: string) {
        this.fillStyle = color;
        return this;
    }

    setStrokeColor(color: string) {
        this.strokeStyle = color;
        return this;
    }

    render() {
        const player = this.getItem();

        const canvasContext = this.getCanvas().getContext();

        canvasContext.strokeStyle = this.getStrokeColor();
        canvasContext.fillStyle = this.getFillColor();

        canvasContext.beginPath();
        canvasContext.arc(player.xPosition, player.yPosition, player.radius, 0, Math.PI * 2, false);
        canvasContext.fill();
        canvasContext.stroke();

        const fontSize = 14;
        canvasContext.font = `${fontSize}px sans-serif`;
        canvasContext.fillStyle = this.getStrokeColor();
        canvasContext.textAlign = "center";
        canvasContext.fillText(player.name, player.xPosition, player.yPosition - player.radius - fontSize/2);
    }
}
