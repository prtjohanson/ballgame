export class Container {
    private services = new Map<string, new (c: Container) => unknown>();

    private instances = new Map<string, unknown>();

    private configs = new Map<string, unknown>();

    constructor() { }

    map<T>(identifier: string, implementation: new (c: Container) => T): Container {
        this.services.set(identifier, implementation);

        return this;
    }

    get<T>(identifier: string, forceNew = false): T {
        const service = this.services.get(identifier);

        if (!service) {
            throw new Error(`Service not found for identifier: ${identifier}`);
        }

        if (!forceNew) {
            const existingInstance = this.instances.get(identifier);

            if (existingInstance) {
                return existingInstance as T;
            }
        }

        const newInstance = this.createInstance(identifier);

        this.instances.set(identifier, newInstance);

        return newInstance as T;
    }

    mapConfig(identifier: string, value: unknown): Container {
        this.configs.set(identifier, value);
        return this;
    }

    getConfig(identifier: string, expectedType: string | undefined = undefined): unknown {
        const config = this.configs.get(identifier);

        if (config === undefined) {
            throw new Error(`Config not found for identifier: ${identifier}`);
        }

        if (expectedType && typeof config !== expectedType) {
            throw new Error(`'${identifier}' expected to be of type '${expectedType}', but '${typeof config}' found.`);
        }

        return config;
    }


    private createInstance<T>(identifier: string): T {
        const service = this.services.get(identifier);

        if (!service) {
            throw new Error(`Service not found for identifier: ${identifier}`);
        }

        return new service(this) as T;
    }
}
