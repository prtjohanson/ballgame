import { App } from "./App";
import { Container } from "./ioc/Container";
import { FieldRenderer } from "./ballgame/renderers/FieldRenderer";
import { GameRenderer } from "./ballgame/renderers/GameRenderer";
import Canvas from "./Canvas";
import { GameStateService } from "./ballgame/services/GameStateService";
import { GoalRenderer } from "./ballgame/renderers/GoalRenderer";
import { BallRenderer } from "./ballgame/renderers/BallRenderer";
import { PlayerRenderer } from "./ballgame/renderers/PlayerRenderer";

const container = (new Container())
    .map('service.gameState', GameStateService)
    .mapConfig('service.gameState.url', '/game')
    .mapConfig('service.gameState.delayMs', 33)

    .map('canvas', Canvas)
    .map('renderer.game', GameRenderer)
    .map('renderer.field', FieldRenderer)
    .map('renderer.goal', GoalRenderer)
    .map('renderer.ball', BallRenderer)
    .map('renderer.player', PlayerRenderer);

const app = new App(container);

function renderLoop() {
    app.render();
    requestAnimationFrame(renderLoop);
}

renderLoop();