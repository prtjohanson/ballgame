import { Container } from "./ioc/Container";

export default class Canvas {
    private readonly canvas: HTMLCanvasElement;
    private readonly context: CanvasRenderingContext2D;

    constructor(container: Container) {
        this.canvas = document.createElement('canvas');

        this.context = this.canvas.getContext('2d') as CanvasRenderingContext2D;

        this.setupCanvas();

        window.addEventListener('resize', () => this.setupCanvas());
    }

    private setupCanvas() {
        const pixelRatio = window.devicePixelRatio || 1;
        const { innerWidth: width, innerHeight: height } = window;

        this.canvas.width = width * pixelRatio;
        this.canvas.height = height * pixelRatio;

        document.body.appendChild(this.canvas);
    }

    public getContext(): CanvasRenderingContext2D {
        return this.context;
    }

    public getWidth() {
        return this.canvas.width;
    }

    public getHeight() {
        return this.canvas.height;
    }
}