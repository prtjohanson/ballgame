import Canvas from "./Canvas";
import { GameStateService } from "./ballgame/services/GameStateService";
import { Game } from "./ballgame/interfaces/Game";
import { Container } from "./ioc/Container";
import {Renderer} from "./ballgame/interfaces/Renderer";

export class App {
    private readonly container: Container;

    private readonly canvas: Canvas;

    private gameStateService: GameStateService;

    private gameRenderer: Renderer<Game>

    private gameStateSet = false;
    constructor(container: Container) {
        this.container = container;

        this.canvas = this.container.get('canvas');

        this.gameStateService = this.container.get('service.gameState');

        this.gameRenderer = this.container.get('renderer.game');

        this.gameRenderer.setCanvas(this.canvas);

        this.gameStateService.getGame().subscribe((game) => {
            this.gameRenderer.setItem(game);
            this.gameStateSet = true;
        });

        this.gameStateService.startFetchingData();
    }
    public render() {
        if (this.gameStateSet) {
            this.gameRenderer.getCanvas().getContext().save();

            this.gameRenderer.render();

            this.gameRenderer.getCanvas().getContext().restore();

        }
    }
}