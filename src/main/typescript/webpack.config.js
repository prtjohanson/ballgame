const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
    entry: './src/index.ts', // Your TypeScript entry point file
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
        filename: 'bundle.js',
        path: __dirname + '/../resources/static/dist',
    },
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin()],
    },
};
