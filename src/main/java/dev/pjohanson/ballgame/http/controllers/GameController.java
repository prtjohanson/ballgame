package dev.pjohanson.ballgame.http.controllers;

import dev.pjohanson.ballgame.GameService;
import dev.pjohanson.ballgame.entities.interfaces.Game;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/game")
public class GameController {

    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping
    public Game getCurrentGameState() {
        return gameService.getGame();
    }
}

