package dev.pjohanson.ballgame.entities.football.movable;

import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Collidable;
import dev.pjohanson.ballgame.entities.interfaces.interactions.IntersectByRay;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Tickable;
import dev.pjohanson.ballgame.entities.interfaces.properties.Friction;
import dev.pjohanson.ballgame.entities.interfaces.properties.Velocity;
import dev.pjohanson.ballgame.entities.interfaces.shapes.Circle;
import dev.pjohanson.ballgame.entities.interfaces.shapes.Rectangle;

abstract public class BaseCircularObject implements Circle, Velocity, Friction, Collidable, Tickable, IntersectByRay {
    public float getVelocity() {
        final float xv = getXVelocity();
        final float yv = getYVelocity();

        return (float)Math.sqrt(xv*xv + yv*yv);
    }

    public float getDistanceTo(float x, float y) {
        final float dx = x - this.getXPosition();
        final float dy = y - this.getYPosition()        ;

        final float d = (float)Math.sqrt(dx*dx + dy*dy);

        return d;
    }

    public float getDistanceTo(Circle c) {
        return this.getDistanceTo(c.getXPosition(), c.getYPosition());
    }

    public float[] getUnitVectorTo(float x, float y) {
        final float dx = x - this.getXPosition();
        final float dy = y - this.getYPosition();

        final float d = (float)Math.sqrt(dx*dx + dy*dy);

        final float ux = dx / d;
        final float uy = dy / d;

        return new float[]{ux, uy};
    }

    public float[] getUnitVectorTo(Circle c) {
        return this.getUnitVectorTo(c.getXPosition(), c.getYPosition());
    }

    public float[] getUnitVectorTo(Rectangle c) {
        float cx = c.getXPosition() + c.getWidth() / 2;
        float cy = c.getYPosition() + c.getHeight() / 2;

        return this.getUnitVectorTo(cx, cy);
    }

    public boolean isColliding(Collidable c) {
        if (c instanceof Circle) {
            final Circle cir = (Circle) c;
            final float radiusSum = cir.getRadius() + this.getRadius();
            final float d = this.getDistanceTo(cir);
            return d < radiusSum;
        }

        if (c instanceof Rectangle) {
            final Rectangle r = (Rectangle) c;

            final float x1 = r.getXPosition();
            final float y1 = r.getYPosition();

            final float x2 = x1 + r.getWidth();
            final float y2 = y1 + r.getHeight();

            final float cx = this.getXPosition();
            final float cy = this.getYPosition();

            return cx > x1 && cx < x2 && cy > y1 && cy < y2;
        }

        return false;
    }

    public Tickable tick(Field field) {
        final float newXPosition = this.getXPosition() + this.getXVelocity();
        final float newYPosition = this.getYPosition() + this.getYVelocity();

        this.setXPosition(newXPosition);
        this.setYPosition(newYPosition);

        return this;
    }

    @Override
    public boolean isRayIntersecting(float ux, float uy, float px, float py) {
        final float cx = this.getXPosition() - px;
        final float cy = this.getYPosition() - py;
        final float r = this.getRadius();

        final float a = ux * ux + uy * uy;
        final float b = 2 * (ux * cx + uy * cy);
        final float c = cx * cx + cy * cy - r * r;

        final float discriminant = b * b - 4 * a * c;

        if (discriminant < 0) {
            return false;
        } else {
            final float t1 = (-b + (float)Math.sqrt(discriminant)) / (2 * a);
            final float t2 = (-b - (float)Math.sqrt(discriminant)) / (2 * a);

            return (t1 >= 0 || t2 >= 0);
        }
    }
}
