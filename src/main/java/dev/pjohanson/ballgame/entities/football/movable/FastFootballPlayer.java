package dev.pjohanson.ballgame.entities.football.movable;

import dev.pjohanson.ballgame.entities.interfaces.movable.Player;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Collidable;

public class FastFootballPlayer extends BasePlayer implements Player {
    private float radius;
    private float xPosition;
    private float yPosition;
    private float xVelocity;
    private float yVelocity;
    private float friction = 0.1f;
    private String name;

    @Override
    public float getRadius() { return radius; }

    @Override
    public FastFootballPlayer setRadius(float radius) {
        this.radius = radius;
        return this;
    }

    @Override
    public float getXPosition() { return xPosition; }

    @Override
    public FastFootballPlayer setXPosition(float xPosition) {
        this.xPosition = xPosition;
        return this;
    }

    @Override
    public float getYPosition() { return yPosition; }

    @Override
    public FastFootballPlayer setYPosition(float yPosition) {
        this.yPosition = yPosition;
        return this;
    }

    @Override
    public float getXVelocity() { return this.xVelocity; }

    @Override
    public FastFootballPlayer setXVelocity(float xVelocity) {
        this.xVelocity = xVelocity;
        return this;
    }

    @Override
    public float getYVelocity() { return yVelocity; }

    @Override
    public FastFootballPlayer setYVelocity(float yVelocity) {
        this.yVelocity = yVelocity;
        return this;
    }

    @Override
    public float getFriction() { return friction; }

    @Override
    public FastFootballPlayer setFriction(float friction) {
        this.friction = friction;
        return this;
    }

    @Override
    public String getName() { return name; }

    @Override
    public FastFootballPlayer setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean isColliding(Collidable c) {
        return super.isColliding(c);
    }

    @Override
    public Player tick(Field field) {
        super.tick(field);

        return this;
    }

    @Override
    public float maxSpeed() {
        return 3.0f;
    }

    @Override
    public float minSpeed() {
        return .5f;
    }
}
