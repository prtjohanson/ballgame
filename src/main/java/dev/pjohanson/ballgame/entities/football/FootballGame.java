package dev.pjohanson.ballgame.entities.football;

import dev.pjohanson.ballgame.entities.interfaces.Game;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;

public class FootballGame implements Game {
    private String blueTeamName;
    private String blackTeamName;
    private Field field;
    private int blueTeamScore;
    private int blackTeamScore;

    @Override
    public String getBlueTeamName() {
        return blueTeamName;
    }

    @Override
    public FootballGame setBlueTeamName(String blueTeamName) {
        this.blueTeamName = blueTeamName;
        return this;
    }

    @Override
    public String getBlackTeamName() {
        return blackTeamName;
    }

    @Override
    public FootballGame setBlackTeamName(String blackTeamName) {
        this.blackTeamName = blackTeamName;
        return this;
    }

    @Override
    public Field getField() {
        return field;
    }

    @Override
    public FootballGame setField(Field field) {
        this.field = field;
        return this;
    }

    @Override
    public int getBlueTeamScore() {
        return blueTeamScore;
    }

    @Override
    public FootballGame setBlueTeamScore(int blueTeamScore) {
        this.blueTeamScore = blueTeamScore;
        return this;
    }

    @Override
    public int getBlackTeamScore() {
        return blackTeamScore;
    }

    @Override
    public FootballGame setBlackTeamScore(int blackTeamScore) {
        this.blackTeamScore = blackTeamScore;
        return this;
    }

    @Override
    public Game tick(Field field) {
        this.field.tick(field);

        final boolean isBlueTeamGoal = field.getBall().isColliding(field.getBlueTeamGoal());

        final boolean isBlackTeamGoal = field.getBall().isColliding(field.getBlackTeamGoal());

        final boolean isOut = !isBlueTeamGoal && !isBlackTeamGoal && !field.getBall().isColliding(field);

        if (isBlueTeamGoal) {
            this.resetBall();
        }

        if (isBlackTeamGoal) {
            this.resetBall();
        }

        if (isOut) {
            this.resetBall();
        }

        return this;
    }

    protected void resetBall() {
        field.getBall().setXVelocity(0);
        field.getBall().setYVelocity(0);
        field.getBall().setXPosition(field.getXPosition() + field.getWidth() / 2.0f);
        field.getBall().setYPosition(field.getYPosition() + field.getHeight() / 2.0f);
    }
}
