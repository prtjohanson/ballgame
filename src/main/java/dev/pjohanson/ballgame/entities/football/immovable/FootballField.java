package dev.pjohanson.ballgame.entities.football.immovable;

import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Collidable;
import dev.pjohanson.ballgame.entities.interfaces.movable.Ball;
import dev.pjohanson.ballgame.entities.interfaces.movable.Player;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Goal;

import java.util.Arrays;

public class FootballField implements Field {
    private float width;
    private float height;
    private float xPosition;
    private float yPosition;
    private float friction;
    private Ball ball;
    private Goal blackTeamGoal;
    private Goal blueTeamGoal;
    private Player[] blackTeamPlayers;
    private Player[] blueTeamPlayers;

    @Override
    public float getWidth() { return width; }

    @Override
    public FootballField setWidth(float width) {
        this.width = width;
        return this;
    }

    @Override
    public float getHeight() { return height; }

    @Override
    public FootballField setHeight(float height) {
        this.height = height;
        return this;
    }

    @Override
    public float getXPosition() { return xPosition; }

    @Override
    public FootballField setXPosition(float xPosition) {
        this.xPosition = xPosition;
        return this;
    }

    @Override
    public float getYPosition() { return yPosition; }

    @Override
    public FootballField setYPosition(float yPosition) {
        this.yPosition = yPosition;
        return this;
    }

    @Override
    public float getFriction() { return friction; }

    @Override
    public FootballField setFriction(float friction) {
        this.friction = friction;
        return this;
    }

    @Override
    public Ball getBall() { return ball; }

    @Override
    public FootballField setBall(Ball ball) {
        this.ball = ball;
        return this;
    }

    @Override
    public Goal getBlackTeamGoal() { return blackTeamGoal; }

    @Override
    public FootballField setBlackTeamGoal(Goal blackTeamGoal) {
        this.blackTeamGoal = blackTeamGoal;
        return this;
    }

    @Override
    public Goal getBlueTeamGoal() { return blueTeamGoal; }

    @Override
    public FootballField setBlueTeamGoal(Goal blueTeamGoal) {
        this.blueTeamGoal = blueTeamGoal;
        return this;
    }

    @Override
    public Player[] getBlackTeamPlayers() { return blackTeamPlayers; }

    @Override
    public FootballField setBlackTeamPlayers(Player[] blackTeamPlayers) {
        this.blackTeamPlayers = blackTeamPlayers;
        return this;
    }

    @Override
    public Player[] getBlueTeamPlayers() { return blueTeamPlayers; }

    @Override
    public FootballField setBlueTeamPlayers(Player[] blueTeamPlayers) {
        this.blueTeamPlayers = blueTeamPlayers;
        return this;
    }

    @Override
    public FootballField tick(Field field) {
        Arrays.stream(blackTeamPlayers).forEach(player -> player.tick(field));

        Arrays.stream(blueTeamPlayers).forEach(player -> player.tick(field));

        ball.tick(field);

        blackTeamGoal.tick(field);

        blueTeamGoal.tick(field);

        return this;
    }

    @Override
    public boolean isColliding(Collidable c) {
        return false;
    }
}

