package dev.pjohanson.ballgame.entities.football.movable;

import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Goal;
import dev.pjohanson.ballgame.entities.interfaces.movable.Ball;
import dev.pjohanson.ballgame.entities.interfaces.movable.Player;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Tickable;

import java.util.Arrays;
import java.util.function.Consumer;

abstract public class BasePlayer extends BaseCircularObject {
    protected Player[] opposingPlayers = null;

    protected Player[] friendlyPlayers = null;

    protected Goal targetGoal = null;


    abstract public float maxSpeed();

    abstract public float minSpeed();

    public Player[] getOpposingPlayers(Field field) {
        if (opposingPlayers == null) {
            populateFieldEntities(field);
        }

        return opposingPlayers;
    }

    public Player[] getFriendlyPlayers(Field field) {
        if (this.friendlyPlayers == null) {
            populateFieldEntities(field);
        }

        return friendlyPlayers;
    }

    public Goal getTargetGoal(Field field) {
        if (targetGoal == null) {
            populateFieldEntities(field);
        }

        return targetGoal;
    }

    public Tickable tick(Field field) {
        final float currentVelocity = this.getVelocity();

        if (currentVelocity < this.minSpeed()) {
            float[] toBallVector = this.getUnitVectorTo(field.getBall());

            this.setXVelocity(toBallVector[0] * this.maxSpeed());
            this.setYVelocity(toBallVector[1] * this.maxSpeed());
        }

        super.tick(field);

        Consumer<Player> playerCollision = anotherPlayer -> {
            if (this.isColliding(anotherPlayer) && this != anotherPlayer) {
                final float vectorToAnotherPlayer[] = this.getUnitVectorTo(anotherPlayer);
                final float nonCollisionDistance = (this.getRadius() + anotherPlayer.getRadius());

                anotherPlayer.setXPosition(this.getXPosition() + vectorToAnotherPlayer[0] * nonCollisionDistance);
                anotherPlayer.setYPosition(this.getYPosition() + vectorToAnotherPlayer[1] * nonCollisionDistance);
            }
        };

        Arrays.stream(field.getBlackTeamPlayers()).forEach(playerCollision);

        Arrays.stream(field.getBlueTeamPlayers()).forEach(playerCollision);

        final float friction = field.getFriction() + this.getFriction();

        this.setXVelocity((float) (this.getXVelocity() * (1.0 - friction)));
        this.setYVelocity((float) (this.getYVelocity() * (1.0 - friction)));

        if (this.isColliding(field.getBall())) {
            final float[] kickVector = this.findKickVector(field);

            final Ball ball = field.getBall();

            ball.setXVelocity(kickVector[0] * 2 * maxSpeed() + maxSpeed() * ((float)Math.random()));
            ball.setYVelocity(kickVector[1] * 2 * maxSpeed() + maxSpeed() * ((float)Math.random()));
        }

        return this;
    }

    protected void populateFieldEntities(Field field)
    {
        if (Arrays.asList(field.getBlackTeamPlayers()).contains(this)) {
            targetGoal = field.getBlueTeamGoal();
            opposingPlayers = field.getBlueTeamPlayers();
            friendlyPlayers = field.getBlackTeamPlayers();
        } else {
            targetGoal = field.getBlackTeamGoal();
            opposingPlayers = field.getBlackTeamPlayers();
            friendlyPlayers = field.getBlueTeamPlayers();
        }
    }



    protected float[] findKickVector(Field field) {
        final float[] vectorToGoal = this.getUnitVectorTo(
                this.getTargetGoal(field)
        );

        return vectorToGoal;
    }
}
