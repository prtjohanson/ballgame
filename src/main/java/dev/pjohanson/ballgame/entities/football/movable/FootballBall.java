package dev.pjohanson.ballgame.entities.football.movable;

import dev.pjohanson.ballgame.entities.interfaces.movable.Ball;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Collidable;

public class FootballBall extends BaseCircularObject implements Ball {
    private float radius;
    private float xPosition;
    private float yPosition;
    private float xVelocity;
    private float yVelocity;
    private float friction = 0.01f;

    @Override
    public float getRadius() { return radius; }

    @Override
    public FootballBall setRadius(float radius) {
        this.radius = radius;
        return this;
    }

    @Override
    public float getXPosition() { return xPosition; }

    @Override
    public FootballBall setXPosition(float xPosition) {
        this.xPosition = xPosition;
        return this;
    }

    @Override
    public float getYPosition() { return yPosition; }

    @Override
    public FootballBall setYPosition(float yPosition) {
        this.yPosition = yPosition;
        return this;
    }

    @Override
    public float getXVelocity() { return xVelocity; }

    @Override
    public FootballBall setXVelocity(float xVelocity) {
        this.xVelocity = xVelocity;
        return this;
    }

    @Override
    public float getYVelocity() { return yVelocity; }

    @Override
    public FootballBall setYVelocity(float yVelocity) {
        this.yVelocity = yVelocity;
        return this;
    }

    @Override
    public float getFriction() { return friction; }

    @Override
    public FootballBall setFriction(float friction) {
        this.friction = friction;
        return this;
    }

    @Override
    public boolean isColliding(Collidable c) {
        return super.isColliding(c);
    }

    @Override
    public Ball tick(Field field) {
        super.tick(field);

        final float friction = field.getFriction() + this.getFriction();

        this.setXVelocity(this.getXVelocity() * (1.0f - friction));
        this.setYVelocity(this.getYVelocity() * (1.0f - friction));

        return this;
    }
}

