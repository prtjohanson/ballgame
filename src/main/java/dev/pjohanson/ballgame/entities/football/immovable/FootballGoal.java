package dev.pjohanson.ballgame.entities.football.immovable;

import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Goal;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Tickable;
import dev.pjohanson.ballgame.entities.interfaces.movable.Ball;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Collidable;

public class FootballGoal implements Goal, Collidable {
    private float width;
    private float height;
    private float xPosition;
    private float yPosition;

    @Override
    public float getWidth() { return width; }

    @Override
    public FootballGoal setWidth(float width) {
        this.width = width;
        return this;
    }

    @Override
    public float getHeight() { return height; }

    @Override
    public FootballGoal setHeight(float height) {
        this.height = height;
        return this;
    }

    @Override
    public float getXPosition() { return xPosition; }

    @Override
    public FootballGoal setXPosition(float xPosition) {
        this.xPosition = xPosition;
        return this;
    }

    @Override
    public float getYPosition() { return yPosition; }

    @Override
    public FootballGoal setYPosition(float yPosition) {
        this.yPosition = yPosition;
        return this;
    }

    @Override
    public boolean isBallInGoal(Ball ball) {
        // TODO: Implement the behavior to check if a ball is in the goal
        return false;
    }

    @Override
    public Tickable tick(Field field) {
        // TODO: Implement the behavior that updates the state of the goal
        return this;
    }

    @Override
    public boolean isColliding(Collidable c) {
        // TODO: Implement the behavior when goal collides with something
        return false;
    }
}
