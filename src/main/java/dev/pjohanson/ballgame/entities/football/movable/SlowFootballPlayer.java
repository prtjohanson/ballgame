package dev.pjohanson.ballgame.entities.football.movable;

import dev.pjohanson.ballgame.entities.interfaces.movable.Player;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Collidable;

public class SlowFootballPlayer extends BasePlayer implements Player {
    private float radius;
    private float xPosition;
    private float yPosition;
    private float xVelocity;
    private float yVelocity;
    private float friction = 0.3f;
    private String name;

    @Override
    public float getRadius() { return radius; }

    @Override
    public SlowFootballPlayer setRadius(float radius) {
        this.radius = radius;

         return this;
    }

    @Override
    public float getXPosition() { return xPosition; }

    @Override
    public SlowFootballPlayer setXPosition(float xPosition) {
        this.xPosition = xPosition;

        return this;
    }

    @Override
    public float getYPosition() { return yPosition; }

    @Override
    public SlowFootballPlayer setYPosition(float yPosition) {
        this.yPosition = yPosition;

        return this;
    }

    @Override
    public float getXVelocity() { return this.xVelocity; }

    @Override
    public SlowFootballPlayer setXVelocity(float xVelocity) {
        this.xVelocity = xVelocity; // The velocity for slow player is constant

        return this;
    }

    @Override
    public float getYVelocity() { return yVelocity; }

    @Override
    public SlowFootballPlayer setYVelocity(float yVelocity) {
        this.yVelocity = yVelocity;

        return this;
    }

    @Override
    public float getFriction() { return friction; }

    @Override
    public SlowFootballPlayer setFriction(float friction) {
        this.friction = friction;

        return this;
    }

    @Override
    public String getName() { return name; }

    @Override
    public SlowFootballPlayer setName(String name) {
        this.name = name;

        return this;
    }

    @Override
    public boolean isColliding(Collidable c) {
        return super.isColliding(c);
    }

    @Override
    public Player tick(Field field) {
        super.tick(field);

        return this;
    }

    @Override
    public float maxSpeed() {
        return 2.0f;
    }

    @Override
    public float minSpeed() {
        return .2f;
    }
}
