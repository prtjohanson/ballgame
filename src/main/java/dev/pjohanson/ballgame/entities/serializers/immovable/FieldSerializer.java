package dev.pjohanson.ballgame.entities.serializers.immovable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;
import java.io.IOException;

public class FieldSerializer extends StdSerializer<Field> {

    public FieldSerializer() {
        this(null);
    }

    public FieldSerializer(Class<Field> t) {
        super(t);
    }

    @Override
    public void serialize(
            Field value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {

        jgen.writeStartObject();

        jgen.writeObjectField("ball", value.getBall());

        jgen.writeObjectField("blackTeamGoal", value.getBlackTeamGoal());
        jgen.writeObjectField("blueTeamGoal", value.getBlueTeamGoal());

        jgen.writeObjectField("blackTeamPlayers", value.getBlackTeamPlayers());
        jgen.writeObjectField("blueTeamPlayers", value.getBlueTeamPlayers());

        // Serialization for inherited properties from Rectangle, Position, and Friction
        jgen.writeNumberField("width", value.getWidth());
        jgen.writeNumberField("height", value.getHeight());

        jgen.writeNumberField("xPosition", value.getXPosition());
        jgen.writeNumberField("yPosition", value.getYPosition());

        jgen.writeNumberField("friction", value.getFriction());

        jgen.writeEndObject();
    }
}
