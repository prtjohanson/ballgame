package dev.pjohanson.ballgame.entities.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import dev.pjohanson.ballgame.entities.interfaces.Game;
import java.io.IOException;

public class GameSerializer extends StdSerializer<Game> {

    public GameSerializer() {
        this(null);
    }

    public GameSerializer(Class<Game> t) {
        super(t);
    }

    @Override
    public void serialize(
            Game value, JsonGenerator gen, SerializerProvider provider)
            throws IOException {

        gen.writeStartObject();
        gen.writeStringField("blueTeamName", value.getBlueTeamName());
        gen.writeStringField("blackTeamName", value.getBlackTeamName());

        gen.writeObjectField("field", value.getField());

        gen.writeNumberField("blueTeamScore", value.getBlueTeamScore());
        gen.writeNumberField("blackTeamScore", value.getBlackTeamScore());
        gen.writeEndObject();
    }
}
