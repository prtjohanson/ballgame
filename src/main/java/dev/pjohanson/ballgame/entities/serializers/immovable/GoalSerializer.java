package dev.pjohanson.ballgame.entities.serializers.immovable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Goal;
import java.io.IOException;

public class GoalSerializer extends StdSerializer<Goal> {

    public GoalSerializer() {
        this(null);
    }

    public GoalSerializer(Class<Goal> t) {
        super(t);
    }

    @Override
    public void serialize(
            Goal value, JsonGenerator gen, SerializerProvider provider)
            throws IOException {

        gen.writeStartObject();

        gen.writeNumberField("width", value.getWidth());
        gen.writeNumberField("height", value.getHeight());

        gen.writeNumberField("xPosition", value.getXPosition());
        gen.writeNumberField("yPosition", value.getYPosition());

        gen.writeEndObject();
    }
}
