package dev.pjohanson.ballgame.entities.serializers.movable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import dev.pjohanson.ballgame.entities.interfaces.movable.Player;
import java.io.IOException;

public class PlayerSerializer extends StdSerializer<Player> {

    public PlayerSerializer() {
        this(null);
    }

    public PlayerSerializer(Class<Player> t) {
        super(t);
    }

    @Override
    public void serialize(
            Player value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {

        jgen.writeStartObject();

        jgen.writeNumberField("radius", value.getRadius());

        jgen.writeNumberField("xPosition", value.getXPosition());
        jgen.writeNumberField("yPosition", value.getYPosition());

        jgen.writeNumberField("xVelocity", value.getXVelocity());
        jgen.writeNumberField("yVelocity", value.getYVelocity());

        jgen.writeNumberField("friction", value.getFriction());

        jgen.writeStringField("name", value.getName());

        jgen.writeEndObject();
    }
}
