package dev.pjohanson.ballgame.entities.interfaces.properties;

public interface Friction {
    float getFriction();
    Friction setFriction(float friction);
}
