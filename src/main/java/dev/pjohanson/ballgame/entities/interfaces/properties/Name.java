package dev.pjohanson.ballgame.entities.interfaces.properties;

public interface Name {
    String getName();
    Name setName(String name);
}
