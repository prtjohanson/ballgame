package dev.pjohanson.ballgame.entities.interfaces.interactions;

public interface Collidable {
    boolean isColliding(Collidable c);
}
