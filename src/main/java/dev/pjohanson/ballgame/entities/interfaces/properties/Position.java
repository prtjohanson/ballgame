package dev.pjohanson.ballgame.entities.interfaces.properties;

public interface Position {
    float getXPosition();
    Position setXPosition(float xPosition);

    float getYPosition();
    Position setYPosition(float yPosition);
}

