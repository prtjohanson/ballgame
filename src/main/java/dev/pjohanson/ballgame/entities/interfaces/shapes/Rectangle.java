package dev.pjohanson.ballgame.entities.interfaces.shapes;

import dev.pjohanson.ballgame.entities.interfaces.properties.Position;

public interface Rectangle extends Position {
    float getWidth();
    Rectangle setWidth(float width);

    float getHeight();
    Rectangle setHeight(float height);
}
