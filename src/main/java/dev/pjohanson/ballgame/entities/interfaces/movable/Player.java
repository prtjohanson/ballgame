package dev.pjohanson.ballgame.entities.interfaces.movable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Collidable;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Tickable;
import dev.pjohanson.ballgame.entities.interfaces.properties.Friction;
import dev.pjohanson.ballgame.entities.interfaces.properties.Name;
import dev.pjohanson.ballgame.entities.interfaces.properties.Position;
import dev.pjohanson.ballgame.entities.interfaces.properties.Velocity;
import dev.pjohanson.ballgame.entities.interfaces.shapes.Circle;
import dev.pjohanson.ballgame.entities.serializers.movable.PlayerSerializer;

@JsonSerialize(using = PlayerSerializer.class)
public interface Player extends Circle, Position, Velocity, Friction, Name, Collidable, Tickable {
}
