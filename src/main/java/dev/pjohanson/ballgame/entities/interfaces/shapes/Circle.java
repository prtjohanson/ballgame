package dev.pjohanson.ballgame.entities.interfaces.shapes;

import dev.pjohanson.ballgame.entities.interfaces.properties.Position;

public interface Circle extends Position {
    float getRadius();
    Circle setRadius(float radius);
}
