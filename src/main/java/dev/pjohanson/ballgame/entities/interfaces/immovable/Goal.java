package dev.pjohanson.ballgame.entities.interfaces.immovable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Collidable;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Tickable;
import dev.pjohanson.ballgame.entities.interfaces.movable.Ball;
import dev.pjohanson.ballgame.entities.interfaces.properties.Position;
import dev.pjohanson.ballgame.entities.interfaces.shapes.Rectangle;
import dev.pjohanson.ballgame.entities.serializers.immovable.GoalSerializer;

@JsonSerialize(using = GoalSerializer.class)
public interface Goal extends Rectangle, Position, Collidable, Tickable {
    boolean isBallInGoal(Ball ball);
}
