package dev.pjohanson.ballgame.entities.interfaces.properties;

public interface Velocity {
    float getXVelocity();
    Velocity setXVelocity(float xVelocity);
    float getYVelocity();
    Velocity setYVelocity(float yVelocity);
}
