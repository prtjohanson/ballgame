package dev.pjohanson.ballgame.entities.interfaces.interactions;

import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;

public interface Tickable {
    Tickable tick(Field field);
}
