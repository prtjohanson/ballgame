package dev.pjohanson.ballgame.entities.interfaces;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Tickable;
import dev.pjohanson.ballgame.entities.interfaces.immovable.Field;
import dev.pjohanson.ballgame.entities.serializers.GameSerializer;

@JsonSerialize(using = GameSerializer.class)
public interface Game extends Tickable {
    String getBlueTeamName();
    Game setBlueTeamName(String blueTeamName);

    String getBlackTeamName();
    Game setBlackTeamName(String blackTeamName);

    Field getField();
    Game setField(Field field);

    int getBlueTeamScore();
    Game setBlueTeamScore(int blueTeamScore);

    int getBlackTeamScore();
    Game setBlackTeamScore(int blackTeamScore);
}