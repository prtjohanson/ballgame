package dev.pjohanson.ballgame.entities.interfaces.immovable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Collidable;
import dev.pjohanson.ballgame.entities.interfaces.movable.Ball;
import dev.pjohanson.ballgame.entities.interfaces.movable.Player;
import dev.pjohanson.ballgame.entities.interfaces.properties.Friction;
import dev.pjohanson.ballgame.entities.interfaces.properties.Position;
import dev.pjohanson.ballgame.entities.interfaces.shapes.Rectangle;
import dev.pjohanson.ballgame.entities.interfaces.interactions.Tickable;
import dev.pjohanson.ballgame.entities.serializers.immovable.FieldSerializer;

@JsonSerialize(using = FieldSerializer.class)
public interface Field extends Rectangle, Position, Friction, Collidable, Tickable {
    Ball getBall();
    Field setBall(Ball ball);

    Goal getBlackTeamGoal();
    Field setBlackTeamGoal(Goal blackTeamGoal);

    Goal getBlueTeamGoal();
    Field setBlueTeamGoal(Goal blueTeamGoal);

    Player[] getBlackTeamPlayers();
    Field setBlackTeamPlayers(Player[] blackTeamPlayers);

    Player[] getBlueTeamPlayers();
    Field setBlueTeamPlayers(Player[] blueTeamPlayers);
}








