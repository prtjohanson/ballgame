package dev.pjohanson.ballgame.entities.interfaces.interactions;

public interface IntersectByRay {
    public boolean isRayIntersecting(float ux, float uy, float px, float py);
}
