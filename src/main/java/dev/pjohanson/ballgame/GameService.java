package dev.pjohanson.ballgame;

import dev.pjohanson.ballgame.entities.interfaces.Game;

import javax.annotation.PostConstruct;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameService {

    private final Game game;

    private final ScheduledExecutorService executorService;

    public GameService(Game game, ScheduledExecutorService executorService) {
        this.game = game;
        this.executorService = executorService;
    }

    @PostConstruct
    public void startGameLoop() {
        final long delay = 1000L / 60; // ~16.67 ms for 60 ticks per second
        executorService.scheduleAtFixedRate(this::tick, 0, delay, TimeUnit.MILLISECONDS);
    }

    public synchronized Game getGame() {
        return this.game;
    }

    private synchronized void tick() {
        game.tick(game.getField());
    }
}
