package dev.pjohanson.ballgame;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:footballgame-config.xml")
public class AppConfig {
}
