package dev.pjohanson.ballgame;

import dev.pjohanson.ballgame.entities.football.FootballGame;
import dev.pjohanson.ballgame.entities.interfaces.Game;
import org.springframework.context.annotation.Bean;

public class GameConfiguration {
    @Bean
    public Game footballGame() {
        return new FootballGame();
    }
}
